#!/usr/bin/python3
# -*- coding: utf-8 -*-

from rewriterFromCSV import RewriterFromCSV
import sys,os
from vocabulary import *
from flight import Flight

def resume(rewritten):
    if len(rewritten) == 0:
        return {}

    length = len(rewritten)
    
    # Initialization of the resume
    resume = {}
    for attrib in rewritten[0].keys() :
        resume[attrib] = {}
        for mod in rewritten[0][attrib].keys():
            resume[attrib][mod] = 0.0
    
    # Add each tuple to the resume
    for i in range(1, len(rewritten)):
        for attrib in rewritten[i].keys():
            for mod in rewritten[i][attrib].keys():
                resume[attrib][mod] += (rewritten[i][attrib][mod])

    # Normalize the resume
    for attrib in resume.keys():
        for mod in resume[attrib].keys():
            resume[attrib][mod]/=length

    return resume


def selective_resume(rewritten, constraint_key_couples, satisfaction_threshold=0):
    if len(rewritten) == 0:
        return {}

    length = len(rewritten)
    
    #Initialization of the resume
    resume = {}
    for attrib in rewritten[0].keys() :
        resume[attrib] = {}
        for mod in rewritten[0][attrib].keys():
            resume[attrib][mod] = 0.0

    for i in range(1, len(rewritten)):
        # Check if the constraints are respected
        constraints_respected = True
        for (constraint_attrib, constraint_mod) in constraint_key_couples:
            if rewritten[i][constraint_attrib][constraint_mod] <= satisfaction_threshold:
                constraints_respected = False
                length-=1
                break

        # Add the tuple to the resume if the constraints are respected
        if constraints_respected:
            for attrib in rewritten[i].keys():
                for mod in rewritten[i][attrib].keys():
                    resume[attrib][mod] += (rewritten[i][attrib][mod])

    # Normalize the resume
    for attrib in resume.keys():
        for mod in resume[attrib].keys():
            resume[attrib][mod]/=length

    return resume

if __name__ == "__main__":
     if len(sys.argv)  < 3:
         print("Usage: python resume.py <vocfile> <dataFile> [--constraints]")
     else:
         if os.path.isfile(sys.argv[1]): 
             voc = Vocabulary(sys.argv[1])
             if os.path.isfile(sys.argv[2]): 
                 rewriter = RewriterFromCSV(voc, sys.argv[2])
                 rewritten = rewriter.readAndRewrite()
                 if len(sys.argv) > 3 and sys.argv[3] == "--constraints":
                     res = selective_resume(rewritten, [("DepTime","morning"),("ArrDelay","early")])
                 else:
                    res = resume(rewritten)
                 print(res)
             else:
                 print("Data file %s not found"%(sys.argv[2]))
         else:
             print("Voc file %s not found"%(sys.argv[2]))
