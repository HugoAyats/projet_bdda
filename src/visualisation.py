#!/usr/bin/python3
# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.font as tkFont
import random

from resume import *
from rewriterFromCSV import *

def display(constraints_list, rewritten_data, previous_win=None):
    if previous_win != None: # Close the previous windows
        previous_win.destroy()
    window = tk.Tk()
    canvas = tk.Canvas(window, height = 600, width = 1200, bg="white")

    # Static texts (titles + constraint list)
    canvas.create_text(600, 20, text="Flights visualisation:", font=tkFont.Font(size = 20))
    canvas.create_text(20, 50, anchor="nw", text="Constraints:")
    canvas.create_text(30, 65, anchor="nw", justify="left", font=tkFont.Font(slant="italic"), text=constraint_text(constraints_list))

    # Button to close the visualisation
    closeButton = tk.Button(window, text="Close", command=window.destroy)
    closeButton.place(x=1100, y=15)

    # Button to remove the last constraint if it exists
    if constraints_list != []:
        previousButton = tk.Button(window, text="Previous", command= lambda: display(constraints_list[:-1], rewritten_data, window))
        previousButton.place(x=15,y=15)
    
    # Computation of the terms that will appear in the word cloud
    resume = selective_resume(rewritten_data,constraints_list)

    key_couple_list = []
    for k in resume:
        for k2 in resume[k]:
            if (k,k2) not in constraints_list:
                key_couple_list.append((k,k2))

    key_couple_list.sort(key=lambda k: resume[k[0]][k[1]], reverse=True)

    top_keys = key_couple_list[:15]
    random.shuffle(top_keys)

    # Creation of the word cloud
    total = 0
    for k1,k2 in top_keys:
        total+= resume[k1][k2]

    def handler(i): # Return the handler for the i° clickable word
        return lambda evt: display(constraints_list + [top_keys[i]], rewritten_data, previous_win=window)

    ypos = 50
    for i in range(len(top_keys)):
        vertical_space = int(resume[top_keys[i][0]][top_keys[i][1]]*530/total)
        text_button = canvas.create_text(xpos(vertical_space), ypos, anchor = "nw", font = tkFont.Font(size=-(vertical_space - 3)), fill=random_color(), text=top_keys[i][0] + ":" + top_keys[i][1])
        ypos += vertical_space
        canvas.tag_bind(text_button, '<1>', handler(i))

    canvas.pack()
    window.mainloop()


def constraint_text(constraints_list):
    if constraints_list == []:
        return "None"

    text = ""
    for cons in constraints_list:
        text+= ("- " + cons[0] + ":" + cons[1] + "\n")
    return text

def random_color():
    colors = ["red", "orange", "yellow", "green", "blue", "indigo", "violet"]
    return random.choice(colors)

def xpos(proportion):
    return random.randint(230, 1200-10*proportion)


if __name__ == "__main__":
    if len(sys.argv)  < 3:
        print("Usage: python visualisation.py <vocfile> <dataFile>")
    else:
        if os.path.isfile(sys.argv[1]): 
            voc = Vocabulary(sys.argv[1])
            if os.path.isfile(sys.argv[2]): 
                rewriter = RewriterFromCSV(voc, sys.argv[2])
                rewritten = rewriter.readAndRewrite()
                display([], rewritten)
            else:
                print("Data file %s not found"%(sys.argv[2]))
        else:
            print("Voc file %s not found"%(sys.argv[2]))
