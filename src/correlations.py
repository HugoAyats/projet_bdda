#!/usr/bin/python3
# -*- coding: utf-8 -*-

from rewriterFromCSV import RewriterFromCSV
import sys,os
from vocabulary import *
from flight import Flight
from resume import *

def dep(term1, term2, data):
    total_resume = resume(data)
    term1_selective_resume = selective_resume(data, [term1])
    (term2_attrib, term2_mod) = term2
    if total_resume[term2_attrib][term2_mod] == 0:
        return -1
    return term1_selective_resume[term2_attrib][term2_mod] / total_resume[term2_attrib][term2_mod]

def assoc(term1, term2, data):
    dependance = dep(term1, term2, data)
    if dependance == -1 :
        return -1
    elif dependance <= 1 :
        return 0
    else:
        return 1 - 1/dependance

def correlated_terms(term1, data, voc):
    correlated_terms_dic = {}
    for attrib in voc.getAttributeNames():
        correlated_terms_dic[attrib] = {}
        for mod in voc.getPartition(attrib).getModNames():
            corr = assoc(term1, (attrib, mod), data)
            if (attrib,mod) != term1 and corr>0 :
                correlated_terms_dic[attrib][mod] = corr
        if len(correlated_terms_dic[attrib].keys()) == 0:
            del correlated_terms_dic[attrib]

    return correlated_terms_dic

def distance(attr, mod1, mod2, resume, voc):
    if voc.getPartition(attr).isEnumPartition() :
        if mod1 == mod2 :
            return 0
        else:
            return 1

    modNames = voc.getPartition(attr).getModNames()
    for i in range(len(modNames)):
        if mod1 == modNames[i]:
            ind_mod1 = i + 1
        if mod2 == modNames[i]:
            ind_mod2 = i + 1

    return abs((ind_mod1 - resume[attr][mod1]) - (ind_mod2 - resume[attr][mod2]))/(len(modname)-1)

def atypicality(resume, voc, term):
    attrib, mod = term
    if resume[attrib][mod] == 0:
        return -1
    return max([min(distance(attrib, mod, mod2, resume, voc),resume[attr][mod2],1-resume[attr][mod]) for mod2 in voc.getPartition(attrib).getModNames()])


if __name__ == "__main__":
    if len(sys.argv)  < 2:
        print("Usage: python correlation.py <vocfile> <dataFile>")
    else:
        if os.path.isfile(sys.argv[1]): 
            voc = Vocabulary(sys.argv[1])
            if os.path.isfile(sys.argv[2]): 
                rewriter = RewriterFromCSV(voc, sys.argv[2])
                rewritten = rewriter.readAndRewrite()
                res = correlated_terms(("DepTime","morning"),rewritten, voc)
                print(res)
            else:
                print("Data file %s not found"%(sys.argv[2]))
        else:
            print("Voc file %s not found"%(sys.argv[2]))
